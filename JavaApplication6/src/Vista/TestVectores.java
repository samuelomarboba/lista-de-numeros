/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.ListaNumeros;
import java.util.Scanner;

/**
 *
 * @author samuel
 */
public class TestVectores {

    public static void main(String[] args) {
        ListaNumeros lista1 = crearLista();
        ListaNumeros lista2 = crearLista();

        /**
         * Como vamos a realizar un proceso de almacenamiento usamos el for
         * convencional
         */
        for (int i = 0; i < lista1.length(); i++) {
            lista1.adicionar(i, leerFloat("Digite dato [" + i + "]:"));
        }
        for (int i = 0; i < lista2.length(); i++) {
            lista2.adicionar(i, leerFloat("Digite dato [" + i + "]:"));
        }
        System.out.println("Su lista 1 es:" + lista1.toString());
        System.out.println("Su lista 2 es: " + lista2.toString());
        try {
            System.out.println("Intersección: " + lista1.getInterseccion(lista2));
        } catch (Exception e) {
            System.out.println("No hay intersección ");
        }
    }

    private static ListaNumeros crearLista() {
        ListaNumeros salida = null;
        try {
            salida = new ListaNumeros(leerEntero("Digite cantidad de elementos\n"));
        } catch (Exception e) {
            System.out.println("No se puede crear un vector cero o negativo");
            return crearLista();
        }
        return salida;
    }

    private static float leerFloat(String msg) {
        System.out.print(msg);
        Scanner in = new Scanner(System.in);
        try {
            return in.nextFloat();
        } catch (java.util.InputMismatchException ex) {
            System.err.println("Error no es un Float");
            return leerFloat(msg);
        }

    }

    private static int leerEntero(String msg) {
        System.out.print(msg);
        Scanner in = new Scanner(System.in);
        try {
            return in.nextInt();
        } catch (java.util.InputMismatchException ex) {
            System.err.println("Error no es un entero");
            return leerEntero(msg);
        }

    }

}
